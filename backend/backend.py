import requests

#Con esa librería, uso la función get y le paso el parámetro que necesito. Guardo las respuestas en una variable json.
parametros = {'query': 'lagunitas'}
resp = requests.get('https://api.openbrewerydb.org/breweries/autocomplete', params=parametros)
json = resp.json()

#Recorro el json y me guardo los IDs de la respuesta.
listaIDs = []
for cerveceria in json:
    listaIDs.append(cerveceria['id'])

#Busco el ID que necesito, que será sólo uno. De nuevo recorriendo el json y guardándome el ID del que quiero.
idBuscado = ''
for elemento in listaIDs:
    resp = requests.get('https://api.openbrewerydb.org/breweries/' + elemento)
    json = resp.json()
    if json['state'] == "California":
        idBuscado = json['id']

resp = requests.get('https://api.openbrewerydb.org/breweries/' + str(idBuscado))
json = resp.json()

#Acá para la parte del reporte armo dos listas vacías
testpassed = []
testfailed = []

#Y empiezo a pasarle tests. Debe haber una forma más prolija de hacer un reporte pero se me ocurrió esto.
#A cada test le asigno un nombre, que si pasa lo sumo a la lista de testpassed, si falla a la de testfailed
test = "ID igual a 761"
if json['id'] == 761:
    testpassed.append(test)
else:
    testfailed.append(test)

test = "Nombre es 'Lagunitas Brewing Co'"
if json['name'] == "Lagunitas Brewing Co":
    testpassed.append(test)
else:
    testfailed.append(test)

test = "Calle es '1280 N McDowell Blvd'"
if json['street'] == "1280 N McDowell Blvd":
    testpassed.append(test)
else:
    testfailed.append(test)

test = "Telefono es 7077694495"
if json['phone'] == "7077694495":
    testpassed.append(test)
else:
    testfailed.append(test)


#Y acá con la función len puedo usar la cantidad de test que pasaron/fallaron. Después imprimiendo las listas tengo los nombres
print("Asserts passed: " + str(len(testpassed)))
print(testpassed)
print("Asserts failed: " + str(len(testfailed)))
print(testfailed)